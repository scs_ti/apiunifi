var pageRefresh
var gclients
var sort_usr

function logout() {
    $.ajax({
        method: "get",
        url: "../php/logout.php",
        success: function() {
            document.location = '../index.php'
        }
    })
}

function get_sites() {
    $.ajax({
        method: "get",
        url: "../php/get_sites.php",
        success: function(data) {
            sites = JSON.parse(data)
            sites.forEach(site => {
                $('ul.dropdown-menu').append('<li><a class="dropdown-item" onclick="change_site('+"'"+ site['name'] +"', '" + site['desc'] + "'"+')">' + site['desc'] + '</a></li>')
            })
            $('ul.dropdown-menu').append('<div class="dropdown-divider"></div>')
            $('ul.dropdown-menu').append('<li><a class="dropdown-item" onclick="logout()">Logout</a></li>')
        }
    })
}

function change_site(site, nsite) {
    $.ajax({
        method: "post",
        url: "../php/change_site.php",
        data: {
            'site': site
        },
        success: function() {
            $('.dropdown-toggle').text(nsite)
            request_clients()
        }
    })
}

function populate_clients(clients) {
    $('#clients').html('')
    if(sort_usr) {
        clients = sort_clients(clients)
    }
    gclients = clients
    i=1
    clients.forEach(client => {
        newRow = document.createElement('div')
        $(newRow).addClass('w-100 d-flex p-3 text-secondary')
        
        if(i>0) {
            $(newRow).addClass('bg-secondary bg-opacity-10')
        }
        
        i = i * (-1)
        
        if(client['1x_identity']) {
            client_name = document.createElement('div')
            $(client_name).attr('style', 'cursor: zoom-in;')
            $(client_name).addClass('col text-center')
            $(client_name).html(client['1x_identity'].toLowerCase())
        } else {
            client_name = document.createElement('div')
            $(client_name).attr('style', 'cursor: zoom-in;')
            $(client_name).addClass('col text-center')
            $(client_name).html('---')
        }
        
        sinal = document.createElement('div')
        $(sinal).addClass('col d-flex align-items-center justify-content-around')
        total = document.createElement('div')
        $(total).attr('style', 'width: 65%; height: 3px; background-color: #CCC; border-radius: .5rem;')
        parcial = document.createElement('div')
        $(parcial).attr('style', 'width: ' + client['satisfaction'] +'%; height: 3px; background-color: #7C7; border-radius: .5rem;')
        $(total).append(parcial)
        $(sinal).html(total)
        $(sinal).append('<small>' + client['satisfaction'] + '%</small>')
        
        mac = document.createElement('div')
        $(mac).addClass('col text-center')
        $(mac).html(client['mac'])

        $(newRow).attr('onclick', 'request_client("' + client['mac'] + '")')
        
        ip = document.createElement('div')
        $(ip).addClass('col text-center')
        $(ip).html(client['ip'])
        
        bytes = client['tx_bytes']
        escala_bytes = ['b', 'Kb', 'Mb', 'Gb', 'Tb']
        n = 0
        while(bytes > 999) {
            bytes = bytes / 1000
            n = n + 1
        }
        bytes = Math.round(bytes)
        bytes = bytes + escala_bytes[n]

        activity = document.createElement('div')
        $(activity).addClass('col text-center')
        $(activity).html(bytes)
        
        conn_time = client['uptime']
        s_time = ''
        if(conn_time / 86400 >= 1) {
            s_time += Math.floor(conn_time / 86400) + 'd '
        }
        conn_time = conn_time % 86400
        if(conn_time / 3600 >= 1) {
            s_time += Math.floor(conn_time / 3600) + 'h '
        }
        conn_time = conn_time % 3600
        if(conn_time / 60 >= 1) {
            s_time += Math.floor(conn_time / 60) + 'm '
        }
        conn_time = conn_time % 60
        s_time += conn_time + 's'

        uptime = document.createElement('div')
        $(uptime).addClass('col text-center')
        $(uptime).html(s_time)
        
        $(newRow).append(client_name)
        $(newRow).append(sinal)
        $(newRow).append(mac)
        $(newRow).append(ip)
        $(newRow).append(activity)
        $(newRow).append(uptime)

        $('#clients').append(newRow)
    });
    
}

function request_clients() {
    
    if(pageRefresh) {
        clearTimeout(pageRefresh)
    }
    
    $.ajax({
        method: "post",
        url: "../php/request_clients.php",
        success: function(data) {
            clients = JSON.parse(data)
            populate_clients(clients)
        }
    })

    pageRefresh = setTimeout(request_clients, 2000)
}

function request_client(chave) {
    found = false
    if(pageRefresh) {
        clearTimeout(pageRefresh)
    }

    if(chave.length != '') {
        if(chave.search(':') > 0) {
            $.ajax({
                method: "post",
                url: "../php/request_clients.php",
                data: {
                    "chave": chave
                },
                success: function(data) {
                    clients = JSON.parse(data)
                    populate_clients(clients)
                }
            })
        } else {
            gclients.forEach( client => {
                if(client['1x_identity'] && client['1x_identity'].toLowerCase() == chave) {
                    found = client
                }
            })
            if(found) {
                populate_clients([found])
            } else {
                alert('Usuário com as credenciais fornecidas, não encontrado!')
                location.reload()
            }
        }

        $('#divform').children('form').children('input').val('')
        $('#divform').children('a').text('Limpar')
        $('#divform').children('a').attr('onclick', 'location.reload()')

        pageRefresh = setTimeout(request_client, 2000, chave)
    } else {
        request_clients()
    }
}

function toggle_sort(param) {
    if(sort_usr == param) {
        sort_usr = false
    } else {
        sort_usr = param
    }

    clearTimeout(pageRefresh)
    request_clients()
}

function sort_clients(clients) {
    clients = clients.sort((a, b) => {
        if(a[sort_usr]){
            if(b[sort_usr]){
                if(a[sort_usr] > b[sort_usr]) {
                    return -1
                } else if(a[sort_usr] < b[sort_usr]) {
                    return 1
                } else {
                    return 0
                }
            } else {
                return -1
            }
        } else {
            return 1
        }
    })
    
    return clients
}