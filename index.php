<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="static/css/main.css">

    <title>UNIFI Controller</title>
</head>
<body class="d-flex flex-column">
    <nav class="navbar navbar-dark d-flex align-items-center justify-content-between px-3">
        <div class="navbar-brand">UNIFI Controller</div>
    </nav>
    <main id="clients" class="flex-fill d-flex align-items-center justify-content-center">
        <form action="php/login.php" method="post" class="form card p-3">
            <?php if(isset($_GET['erro']) && $_GET['erro'] == 400) { ?>
                <p class="alert alert-danger">Credenciais inválidas!</p>
            <?php } else if(isset($_GET['erro']) && $_GET['erro'] == 403) { ?>
                <p class="alert alert-warning">Efetue login!</p>
            <?php } ?>
            <fieldset class="form-group">
                <label class="form-control-label">Usuário</label>
                <input class="form-control my-2" type="text" name="usuario" id="usuario" required>
            </fieldset>
            <fieldset class="form-group">
                <label class="form-control-label">Senha</label>
                <input class="form-control my-2" type="password" name="senha" id="senha" required>
            </fieldset>
            <button type="submit" class="mt-3 btn btn-block btn-outline-secondary">Entrar</button>
        </form>
    </main>
</body>
</html>