<?PHP

    require_once '../vendor/autoload.php';

    session_start();
    if(!isset($_SESSION['logged'])) {
        header('Location: ../index.php?erro=403');
    }
    
    $conn = new UniFi_API\Client($_SESSION['usuario'], $_SESSION['senha'], $_SESSION['host'], $_SESSION['site']);
    $conn->login();
    
    $sites = $conn->list_sites();
    
    echo json_encode($sites)

?>