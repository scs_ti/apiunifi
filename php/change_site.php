<?PHP

    require_once '../vendor/autoload.php';

    session_start();
    if(!isset($_SESSION['logged'])) {
        header('Location: ../index.php?erro=403');
    }
    
    $_SESSION['site'] = $_POST['site'];

?>