<?PHP

    require_once '../vendor/autoload.php';

    session_start();
    if(!isset($_SESSION['logged'])) {
        header('Location: ../index.php?erro=403');
    }
    
    $conn = new UniFi_API\Client($_SESSION['usuario'], $_SESSION['senha'], $_SESSION['host'], $_SESSION['site']);
    $conn->login();
    
    if(isset($_POST['chave']))
        $usr = $conn->list_clients($_POST['chave']);
    else
        $usr = $conn->list_clients();
    
    echo json_encode($usr)

?>