<?php 
    session_start();
    if(!isset($_SESSION['logged'])) {
        header('Location: ../index.php?erro=403');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    
    
    <link rel="stylesheet" href="../static/css/main.css">
    <script src="../js/main.js"></script>

    <title>UNIFI Controller</title>
</head>
<body class="d-flex flex-column">
    <nav class="navbar navbar-dark bg-dark bg-gradient d-flex align-items-center justify-content-between px-3">
        <div class="btn-group">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                ASSOCIACAO
            </button>
            <ul class="dropdown-menu">
            </ul>
        </div>
        <div class="d-flex" id="divform">
            <form class="form mx-3">
                <input type="text" class="form-control" placeholder="usuário ou mac">
            </form>
            <a class="btn btn-secondary" onclick="request_client($(this).siblings('form').children('input').val())">Buscar</a>
        </div>
    </nav>
    <div class="d-flex px-3 py-2" style="background-image: linear-gradient(to bottom, #777, #999);">
        <div class="col text-center text-white" style="cursor: pointer;" onclick="toggle_sort('1x_identity')">Usuário</div>
        <div class="col text-center text-white" style="cursor: pointer;" onclick="toggle_sort('satisfaction')">Sinal</div>
        <div class="col text-center text-white" style="cursor: pointer;" onclick="toggle_sort('mac')">Mac</div>
        <div class="col text-center text-white" style="cursor: pointer;" onclick="toggle_sort('ip')">IP</div>
        <div class="col text-center text-white" style="cursor: pointer;" onclick="toggle_sort('tx_bytes')">Atividade</div>
        <div class="col text-center text-white" style="cursor: pointer;" onclick="toggle_sort('uptime')">Tempo conectado</div>
        <div class="text-center text-white">&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <main id="clients" class="flex-fill d-flex flex-column align-items-center overflow-scroll"></main>
    <script>
        get_sites()
        request_clients()
    </script>
</body>
</html>